# Utilisez une image de base appropriée
FROM python:3.10.13-alpine3.18

# Copiez le contenu du dossier app/ dans /app/
COPY app /app/
COPY requirements.txt /tmp/pip/

# Définissez le répertoire de travail
WORKDIR /app

RUN pip install -r /tmp/pip/requirements.txt

# Exécutez le fichier modules.sh
CMD ["python3","/app/app.py"]


