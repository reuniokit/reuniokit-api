from models.postgre_database import Dataclass
#import boto3
import chardet
import pandas as pd
import timeit
import os

from pathlib import Path

# Supprimer toutes les tables pour réinitialiser la bdd
def delete_all_tables_initilisation():
    Dataclass.delete_table('login')
    Dataclass.delete_table('fiche')
    Dataclass.delete_table('fiche_pdf')


# Définition et création des tables de la BDD
def create_all_tables_initialisation():
    #Définition des colonnes pour chaque table
    login_colonnes = [("id_login", "SERIAL PRIMARY KEY"), ("identifiant", "TEXT"), ("mot_de_passe", "TEXT"), ("direcion", "TEXT"), ("service", "TEXT")]
    fiche_colonnes = [("id_fiche", "SERIAL PRIMARY KEY"), ("nom_fiche", "TEXT"), 
                        ("typologie_service", "TEXT"), ("typologie_information", "TEXT"), ("typologie_construction", "TEXT"), ("typologie_decision", "TEXT"), 
                        ("objectif_ordre", "TEXT"), ("objectif", "TEXT"), ("objectif_additionnel", "TEXT"), ("objectif_brise_glace", "TEXT"),
                        ("phase_ordre", "TEXT"), ("phase", "TEXT"), 
                        ("duree_min", "TEXT"), ("duree_max", "TEXT"), ("duree_par_participant", "TEXT"), ("participant_min", "TEXT"), ("participant_max", "TEXT"),
                        ("faisable_visio", "TEXT"), ("difficulte", "TEXT"), ("contexte_detendu", "TEXT"), ("besoin_materiel", "TEXT"),
                        ("description_courte", "TEXT"), ("description", "TEXT"), ("preparation", "TEXT"), 
                        ("deroulement", "TEXT"), ("points_de_vigilance", "TEXT"), ("conseil", "TEXT"), ("option", "TEXT"), ("a_savoir", "TEXT"), ("debrief", "TEXT")]
    fiche_pdf_colonnes =  [("id_fiche_pdf", "SERIAL PRIMARY KEY"), ("id_fiche", "INT"), ("dossier", "TEXT"), ("nom_fichier_pdf", "TEXT")]

            
    # Créer les tables dans la BDD
    Dataclass.create_table("login", login_colonnes)
    Dataclass.create_table("fiche", fiche_colonnes)
    Dataclass.create_table("fiche_pdf", fiche_pdf_colonnes)



#création de login de test - si une table login est necessaire
def creation_login():
    login = {"identifiant": 'test',  "mot_de_passe": "test"}
    login2 = {"identifiant": 'test',  "mot_de_passe": "test"}

    # Générer les identifiants - a retirer pour la production
    Dataclass.insert_data('login', login)
    Dataclass.insert_data('login', login2)


# Fonction pour trouver la fiche correspondante
def ajout_url_fiche():
    #Ajout des lignes dans la table fiche_pdf
    fiche_pdf_colonnes =  ["id_fiche_pdf", "id_fiche", "dossier", "nom_fichier_pdf"]
    pathlist = Path("/home/onyxia/work/reuniokit-api/app/reuniokit").glob('**/*.pdf')

    for path in pathlist:
        # because path is object not string
        path_in_str = str(path)
        #print(path_in_str)
        splited_path = path_in_str.split("/")
        filename = splited_path[-1]
        #print(filename)
        filename_sans_extension = filename[:-4]
        dossier = splited_path[7]
        if dossier == "1. Briser la glace":
            dossier = dossier + "/" + splited_path[8]
        Dataclass.insert_data('fiche_pdf', {"dossier": dossier, "nom_fichier_pdf": filename_sans_extension})
        print("Ligné insérée")



"""
Initialisation des données dans la BDD à partir d'un fichier source.
A adapter en fonction des projets
"""
def ajout_data(df_source):
    fiche_colonnes = ["nom_fiche", 
                        "typologie_service", "typologie_information", "typologie_construction", "typologie_decision", 
                        "objectif_ordre", "objectif", "objectif_additionnel", "objectif_brise_glace",
                        "phase_ordre", "phase", 
                        "duree_min", "duree_max", "duree_par_participant", "participant_min", "participant_max",
                        "faisable_visio", "difficulte", "contexte_detendu", "besoin_materiel", 
                        "description_courte", "description", "preparation", "deroulement", "points_de_vigilance", "conseil", "option", "a_savoir", "debrief"]

    df_source_clean = df_source.drop(df_source.columns[[0, 1, 2, 3, 15]], axis=1)
    df_source_clean.reset_index(drop=True, inplace=True)
    
    nom_colonnes = list(df_source_clean.keys())
    print(nom_colonnes)
    print(len(nom_colonnes), len(fiche_colonnes))

    for i in range(len(df_source_clean)):
        row_to_insert = {}
        for num_colonne in range(len(fiche_colonnes)):
            row_to_insert[fiche_colonnes[num_colonne]] = str(df_source_clean.loc[i, nom_colonnes[num_colonne]])
        Dataclass.insert_data('fiche', row_to_insert)
        print(f"Ligne {i+1} inseree dans la table Fiche")
    
    ajout_url_fiche()
    data_fiches = Dataclass.select_data("fiche")
    for row in data_fiches:
        print(row["nom_fiche"])
        data_fiche_url = Dataclass.select_single_data_by_specific_column("fiche_pdf", "nom_fichier_pdf", row["nom_fiche"], correspondace_exacte=True)
        print(data_fiche_url)
        Dataclass.update_data("fiche_pdf", {"id_fiche":row["id_fiche"]} ,data_fiche_url[0]["id_fiche_pdf"])






if __name__ == "__main__":
    """ 
        Generation a partir du fichier Excel des fichiers CSV correspondant à chaque feuille du fichier EXCEL
        Cette étape permet de gérer les problèmes d'encoding
    """
    FILE_PATH = "/home/onyxia/work/reuniokit-api/app/Base de données Lab de Transfos.xlsx"
    with open(FILE_PATH, mode="rb") as file_in:
        df_reuniokit_source = pd.read_excel(file_in, sheet_name=None, keep_default_na=False)

    onglets = df_reuniokit_source.keys()
    for onglet in onglets:
        df_reuniokit_source[onglet].to_csv(f"reuniokit_{onglet}.csv",sep=",", encoding='cp1252')

    data_source = pd.read_csv(f"reuniokit_{list(onglets)[0]}.csv", encoding="latin1", skiprows=1, keep_default_na=False).reset_index(drop=True)
    print(data_source.head())

    #Lancement de toutes les méthodes
    delete_all_tables_initilisation()
    create_all_tables_initialisation()
    ajout_data(data_source)
    creation_login()
    #log_delete_data()

    #print(Dataclass.select_data('login'))
    #print(Dataclass.select_data('fiche'))
    #print(Dataclass.select_data('fiche_pdf'))

    for onglet in onglets:
        os.remove(f"reuniokit_{onglet}.csv")
