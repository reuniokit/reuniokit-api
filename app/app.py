from flask import Flask, render_template, session, request, make_response
from flask_cors import CORS, cross_origin
from werkzeug.exceptions import HTTPException
from waitress import serve
import json
import secrets
from models.login import login_routes
from models.routes_fiches import routes_fiches
from models.routes_referentiel import routes_referentiel
from models.log_listener import setup_log_event_handlers
from models.observer_pattern import post_event

app = Flask(__name__)
app.json.sort_keys = False

#app.register_blueprint(login_routes)
app.register_blueprint(routes_fiches, url_prefix='/v1')
app.register_blueprint(routes_referentiel, url_prefix='/v1')

CORS(app, resources={r"/*": {"origins": "*"}})
app.secret_key= 'Mzhghfvhgg32P'
app.config['PERMANENT_SESSION_LIFETIME']= 1600

# Initialisation des listeners
setup_log_event_handlers()
 
@app.route("/", methods=['GET'])
def home():
    resp = make_response({"response":"API de l'application Réuniokit ! V1.0"})
    return resp

@app.route("/send_data", methods=['PUT'])
def send_data():
    if request.method == 'PUT':
        data = request.get_json()
        print(data)
        print({"ip": request.remote_addr})
        resp = make_response({"response":"Successful"}, 200)
        
        return resp
    else:
        resp = make_response({"response":"Unable to realise such action"}, 404)

@app.before_request
def avant_requete():
    user = request.remote_addr
    url_root = request.url_root
    url_full_path = request.full_path
    methode = request.method
    data_to_log = {"Type de requete" : "DEMANDE",
                "url": url_root,
                "Route": url_full_path,
                "Code HTTP": 100,
                "Methode HTTP": methode,
                "Reponse": json.dumps({}),
                "Body": json.dumps({}),
                "Erreur description": "",
                "Utilisateur" : user
                }
    if methode == "POST":
        data_to_log["Body"] = json.dumps(request.get_json())

    if url_full_path != "/?":
        post_event("user_action", data_to_log)


@app.after_request
def add_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'

    user = request.remote_addr
    url_root = request.url_root
    url_full_path = request.full_path
    methode = request.method
    data_to_log = {"Type de requete" : "REPONSE",
                "url": url_root,
                "Route": url_full_path,
                "Code HTTP": response.status_code,
                "Methode HTTP": methode,
                "Reponse": json.dumps({}),
                "Body": json.dumps({}),
                "Erreur description": response.status,
                "Utilisateur" : user
                }

    try:
        data_to_log["Reponse"] = json.dumps(response.data.strip().decode('utf-8'))
    except RuntimeError as e:
        data_to_log["Reponse"] = json.dumps({"fichier": "PDF"})
    except Exception as e:
        data_to_log["Erreur description"] = e
    
    if url_full_path != "/?":
        post_event("user_action", data_to_log)
    return response


# Gestion des erreurs
@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    
    user = request.remote_addr
    url_root = request.url_root
    url_full_path = request.full_path
    methode = request.method
    data_to_log = {"Type de requete" : "REPONSE",
                "url": url_root,
                "Route": url_full_path,
                "Code HTTP": e.code,
                "Methode HTTP": methode,
                "Reponse": json.dumps({}),
                "Body": json.dumps({}),
                "Erreur description": e.description,
                "Utilisateur" : user
                }
    post_event("user_action", data_to_log)
    return make_response(response)

mode = "dev"

if __name__ == '__main__':
    if mode == "dev":
        app.run(host='0.0.0.0', port=5050, debug=True)
    else:
        serve(app, host='0.0.0.0', port=5050, threads=10)

