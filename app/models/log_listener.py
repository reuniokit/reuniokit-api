from datetime import datetime
from zoneinfo import ZoneInfo

from models.postgre_database import Dataclass
from models.observer_pattern import subscribe

# data_logs est un dict. data_logs doit contenir 2 clés: "url" et "data"
def handle_any_user_event(data_logs):
    Dataclass.insert_data("logs", 
                        {"date_envoi" : datetime.now(tz=ZoneInfo("Europe/Paris")).strftime("%Y/%m/%d %H:%M:%S"), 
                            "type_requete" : data_logs["Type de requete"],
                            "url": data_logs["url"],
                            "route": data_logs["Route"],
                            "code_http": data_logs["Code HTTP"],
                            "methode_http": data_logs["Methode HTTP"],
                            "body": data_logs["Body"],
                            "reponse": data_logs["Reponse"],
                            "erreur_description": data_logs["Erreur description"],
                            "utilisateur" : data_logs["Utilisateur"]
                        })

def setup_log_event_handlers():
    subscribe("user_action", handle_any_user_event)


