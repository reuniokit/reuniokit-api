import os

config = os.environ

""" db informations """
table_id = {"login":"id_login", 
        "fiche":"id_fiche",
        "fiche_pdf":"id_fiche_pdf"
}

groupby_operation = {"somme" : "SUM",
    "moyenne" : "AVG",
    "min" : "MIN",
    "max" : "MAX",
    "nombre" : "COUNT"
}