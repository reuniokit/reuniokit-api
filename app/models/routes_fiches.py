from flask import Blueprint, session, request, redirect, make_response, send_file
from flask_cors import cross_origin

import json
import pandas as pd
import s3fs
import os
from io import BytesIO

from models.db_config import *
from models.postgre_database import Dataclass
from models.proposition_fiches_algo import proposition_fiche


routes_fiches = Blueprint('routes_fiches', __name__)

'''
Les fonctions suivantes doivent renvoyer uniquement des données
au format json
NB: make_response convertit les données en format json automatiquement
'''

@routes_fiches.route("/fiches", methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type','access-control-allow-origin'])
def get_all_fiches():
    fiches = Dataclass.select_data("fiche")
    resp = make_response(fiches)
    return resp

@routes_fiches.route("/fiche/<string:id_fiche>", methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type','access-control-allow-origin'])
def get_specific_fiche(id_fiche):
    fiche = Dataclass.select_single_data("fiche", id_fiche)

    return make_response(fiche)

@routes_fiches.route("/fiche/download/<string:id_fiche>", methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type','access-control-allow-origin'])
def get_pdf_specific_fiche(id_fiche):
    dirname, filename = os.path.split(os.path.abspath(__file__))
    cert_file = "/incubateur-finances-rie-gouv-fr.pem"
    # Create filesystem object
    fs = s3fs.S3FileSystem(
        key = config["ACCESS_KEY"], 
        secret = config["SECRET_KEY"], 
        client_kwargs={
            'endpoint_url': 'https://minio.lab.incubateur.finances.rie.gouv.fr',
            }
        )

    fiche_pdf = Dataclass.select_single_data_by_specific_column("fiche_pdf", "id_fiche", id_fiche)
    BUCKET = "ytihianine/reuniokit/"
    dossier = fiche_pdf[0]["dossier"] + "/"
    nom_fiche = fiche_pdf[0]["nom_fichier_pdf"] +".pdf"

    file_path = BUCKET + dossier + nom_fiche

    try:
        # Récupérer le fichier depuis MinIO
        with fs.open(file_path, 'rb') as file:
            bytes_stream = BytesIO(file.read())

        return send_file(bytes_stream, as_attachment=True, download_name=nom_fiche)

    except Exception as e:
        return make_response({"erreur": f"Erreur lors de la récupération du fichier depuis MinIO: {e}",
                               "MINIO KEY ID" : config["ACCESS_KEY"],
                               "MINIO SECRET KEY": config["SECRET_KEY"]}, 500)


@routes_fiches.route("/fiches/proposition", methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type','access-control-allow-origin'])
def fiches_proposition():
    # Récupérer les variables de notre requête
    try:
        data_to_send = request.get_json()
    except Exception as e:
        return make_response({"erreur" : f"Aucune données envoyées : {e}"}, 400)


    # Le nombre de participant est obligatoires
    if data_to_send['nb_participant'] == '':
        return make_response({"erreur": "Veuillez renseigner un nombre de participant"}, 400)

    # Formattage des variables
    if data_to_send['duree_reunion'] != '':
        data_to_send['duree_reunion']= int(data_to_send['duree_reunion'])
    data_to_send['nb_participant']= int(data_to_send['nb_participant'])

    data_to_send['type_reunion']= data_to_send['type_reunion'].replace("é", "e")
    data_to_send['type_objectifs'] = [objectif.strip() for objectif in data_to_send['type_objectifs']]

    # Ajout des objectifs obligatoires - les mettre dans l'ordre
    data_to_send['type_objectifs'] = ["Briser la glace"] + data_to_send['type_objectifs'] + ["Clôturer"]
    objectifs_dans_lordre = ["Briser la glace", "Partager la vision", "Résoudre un problème", 
                                "Générer des idées", "Prendre une décision", "Formaliser", "Répartir les tâches",
                                 "Prototyper", "Faire le bilan", "Clôturer"]
    used_objectif_dans_lordre = []
    for objectif in objectifs_dans_lordre:
        if objectif in data_to_send["type_objectifs"]:
            used_objectif_dans_lordre.append(objectif)

    data_to_send['type_objectifs'] = used_objectif_dans_lordre
    # Générer une proposition
    proposition = proposition_fiche(data_to_send)
    return make_response(proposition, 200)


"""
Cette route permet d'obtenir les objectifs possibles d'une réunion
en fonction de la typologie de la réunion
"""
@routes_fiches.route("/objectifs/<string:typologie_reunion>", methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type','access-control-allow-origin'])
def get_objectifs(typologie_reunion):
    # Formatage des données
    typologie_reunion = typologie_reunion.replace("é", "e")
    fiches_reunion = pd.DataFrame(Dataclass.select_data_condition("fiche", {f"typologie_{typologie_reunion.lower()}":"<> ''"}))
    # Convertir en numérique les valeurs pour les trier
    fiches_reunion['objectif_ordre'] = pd.to_numeric(fiches_reunion['objectif_ordre'])
    fiches_reunion = fiches_reunion.sort_values(by=['objectif_ordre'])

    # Créer une liste qui contient les objectifs possibles
    objectifs = [objectif for objectif in list(fiches_reunion["objectif"].unique()) if objectif != '']
    objectifs_brise_glace = [objectif_brise_glace for objectif_brise_glace in list(fiches_reunion["objectif_brise_glace"].unique()) if objectif_brise_glace != '']

    # Structure des données envoyées
    data_to_send = {"objectifs": [], "objectifs_brise_glace": []}
    data_to_send["objectifs"] = objectifs
    data_to_send["objectifs_brise_glace"] = objectifs_brise_glace

    resp = make_response(data_to_send)
    return resp


# Modifier une donnée dans une table specifique
@routes_fiches.route('/update/<string:table>/<string:id_data>', methods=['POST'])      
def update_single_data_from_table(table, id_data):
    return {"data": 0}

# Supprimer une donnée dans une table specifique
@routes_fiches.route('/delete/<string:table>/<string:id_data>', methods=['POST'])      
def delete_single_data_from_table(table, id_data):
    return {"data": 0}

# Ajouter plusieurs données dans une table specifique
@routes_fiches.route('/add/<string:table>', methods=['POST'])      
def add_data_from_table(table):
    return {"data": 0}


# Test des variables d'environnement
@routes_fiches.route("/environ", methods=['GET'])
def environ():
    return make_response(dict(config), 200)

