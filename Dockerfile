# Utilisez une image de base appropriée
FROM python:3.10.10-slim-buster

# Copiez le contenu du dossier app/ dans /app/
COPY app /app/
COPY pip /tmp/pip/

# Ajout des certificats MinIO
COPY bercyCA.crt /tmp

# Définissez le répertoire de travail
WORKDIR /app

RUN pip install --no-index -r /tmp/pip/requirements.txt --find-links=/tmp/pip/

# Ajout de la partie manquante du certificat
RUN ls /usr/local/lib/
RUN cat /usr/local/lib/python3.10/site-packages/certifi/cacert.pem > /tmp/ca_tmp.crt
RUN cat /tmp/ca_tmp.crt /tmp/bercyCA.crt > /usr/local/lib/python3.10/site-packages/certifi/cacert.pem


# Exécutez le fichier modules.sh
CMD ["python3","/app/app.py"]


